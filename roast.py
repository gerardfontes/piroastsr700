#!/usr/bin/python

import sys
from time import time
from time import sleep
import multiprocessing
import freshroastsr700
import progressbar

# freshroastsr700 uses multiprocessing under the hood.
# call multiprocessing.freeze_support() if you intend to
# freeze your app for packaging.
multiprocessing.freeze_support()

# Create a roaster object.
roaster = freshroastsr700.freshroastsr700(thermostat=True,ext_sw_heater_drive=False)
step = 0

# Conenct to the roaster.
print "Connecting to roaster.."
roaster.auto_connect()
while 1:
    if roaster.connected:
        break
    else:
        sleep(1)
print "Connected."

#define functions
def display_progress(duration):
    bar = progressbar.ProgressBar(maxval=duration, widgets=[progressbar.Bar('=', '[', ']'), ' ', progressbar.ETA()])
    bar.start()
    starttime = time()
    elapsedtime = 0
    while elapsedtime < duration:
        bar.update(elapsedtime)
        sleep(0.1)
        elapsedtime = time() - starttime
    bar.finish()

def roast(temperature,speed,duration):
    global step
    print "Step", step
    step += 1
    roaster.fan_speed = speed
    roaster.target_temp = temperature
    roaster.time_remaining = duration
    if roaster.get_roaster_state() != "roasting":
        roaster.roast()
    display_progress(duration)

def cooldown():
    print "Cooling down.."
    roaster.fan_speed = 9
    roaster.time_remaining = 180
    roaster.cool()
    display_progress(180)

#do we have a warmup?
if len(sys.argv) > 1:
    print "Warming up.."
    roast(250,9,60)
    print "Warmup complete."
else:
    print "Warmup skipped"

#variable to display current step
step = 1

#steps
roast(275,9,60)
roast(300,9,60)
roast(325,9,60)
roast(350,9,60)
roast(400,9,60)
roast(425,9,120)
roast(450,9,120)
roast(475,9,120)
roast(500,4,110)

#cooldown
cooldown()

# Disconnect from the roaster.
roaster.sleep()
roaster.disconnect()
print "Done."
