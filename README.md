# PiRoastSR700

Use the raspberry pi 3 to remotely control your FreshRoast SR700 roaster using wifi.

Based on freshroastsr700 python module at https://pypi.org/project/freshroastsr700/

Provides a basic layout with all the required steps to roast your coffee, it will 
display every step with a progress bar and ETA.

I recommend using the latest raspbian since other distributions like ubuntu have a
very unstable wifi control, with raspbian the wifi is flawless.